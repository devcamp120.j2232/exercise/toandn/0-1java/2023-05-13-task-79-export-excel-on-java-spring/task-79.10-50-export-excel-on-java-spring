package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365.entity.Product;

@Repository
public interface IProductRepository extends JpaRepository<Product, Integer> {
	boolean existsByProductCode(String productCode);
}
