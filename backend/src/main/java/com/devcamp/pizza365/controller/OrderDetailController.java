package com.devcamp.pizza365.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.OrderDetail;
import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.repository.IOrderRepository;
import com.devcamp.pizza365.repository.IProductRepository;
import com.devcamp.pizza365.repository.IOrderDetailRepository;
import com.devcamp.pizza365.service.OrderDetailService;

@RestController
@CrossOrigin
@RequestMapping("/OrderDetailDetails")
public class OrderDetailController {
  @Autowired
  IOrderDetailRepository gIOrderDetailRepository;
  @Autowired
  IOrderRepository gIOrderRepository;
  @Autowired
  IProductRepository gIProductRepository;
  @Autowired
  OrderDetailService gOrderDetailService;

  @GetMapping("/all")
  public ResponseEntity<List<OrderDetail>> getAllOrderDetail() {
    try {
      return new ResponseEntity<>(gOrderDetailService.getAllOrderDetail(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/create/{OrderId}/{ProductId}")
  public ResponseEntity<Object> createOrderDetail(@PathVariable Integer OrderId, @PathVariable Integer ProductId,
      @Valid @RequestBody OrderDetail paramOrderDetail) {
    Optional<Order> vOrderData = gIOrderRepository.findById(OrderId);
    Optional<Product> vProductData = gIProductRepository.findById(ProductId);
    if (vOrderData.isPresent() && vProductData.isPresent()) {
      try {
        return new ResponseEntity<>(gOrderDetailService.createOrderDetail(paramOrderDetail, vOrderData, vProductData),
            HttpStatus.CREATED);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Create specified OrderDetail: " + e.getCause().getCause().getMessage());
      }
    } else {
      Order vOrderNull = new Order();
      return new ResponseEntity<>(vOrderNull, HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping("/update/{id}/{OrderId}/{ProductId}")
  public ResponseEntity<Object> updateOrderDetail(@PathVariable Integer id, @PathVariable Integer OrderId,
      @PathVariable Integer ProductId,
      @Valid @RequestBody OrderDetail paramOrderDetail) {
    Optional<OrderDetail> vOrderDetailData = gIOrderDetailRepository.findById(id);
    if (vOrderDetailData.isPresent()) {
      Optional<Order> vOrderData = gIOrderRepository.findById(OrderId);
      Optional<Product> vProductData = gIProductRepository.findById(ProductId);
      if (vOrderData.isPresent() && vProductData.isPresent()) {
        try {
          return new ResponseEntity<>(
              gOrderDetailService.updateOrderDetail(paramOrderDetail, vOrderDetailData, vOrderData, vProductData),
              HttpStatus.OK);
        } catch (Exception e) {
          return ResponseEntity.unprocessableEntity()
              .body("Failed to Update specified OrderDetail: " + e.getCause().getCause().getMessage());
        }
      } else {
        Order vOrderNull = new Order();
        return new ResponseEntity<>(vOrderNull, HttpStatus.NOT_FOUND);
      }
    } else {
      OrderDetail vOrderDetailNull = new OrderDetail();
      return new ResponseEntity<>(vOrderDetailNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deleteOrderDetailById(@PathVariable Integer id) {
    Optional<OrderDetail> vOrderDetailData = gIOrderDetailRepository.findById(id);
    if (vOrderDetailData.isPresent()) {
      try {
        gIOrderDetailRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      OrderDetail vOrderDetailNull = new OrderDetail();
      return new ResponseEntity<>(vOrderDetailNull, HttpStatus.NOT_FOUND);
    }
  }
}
