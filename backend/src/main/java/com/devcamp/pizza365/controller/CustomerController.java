package com.devcamp.pizza365.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.ICustomerRepository;
import com.devcamp.pizza365.service.CustomerService;
import com.devcamp.pizza365.service.ExcelExporter;

@RestController
@CrossOrigin
@RequestMapping("/customers")
public class CustomerController {
	@Autowired
	ICustomerRepository gICustomerRepository;
	@Autowired
	CustomerService gCustomerService;

	@GetMapping("/last-name/{lastName}")
	public ResponseEntity<List<Customer>> getCustomersByLastNameLike(@PathVariable String lastName) {
		try {
			List<Customer> vCustomers = new ArrayList<Customer>();
			gICustomerRepository.findByLastNameLike(lastName).forEach(vCustomers::add);
			return new ResponseEntity<>(vCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/first-name/{firstName}")
	public ResponseEntity<List<Customer>> getCustomersByFirstNameLike(@PathVariable String firstName) {
		try {
			List<Customer> vCustomers = new ArrayList<Customer>();
			gICustomerRepository.findByFirstNameLike(firstName).forEach(vCustomers::add);
			return new ResponseEntity<>(vCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/city/{city}")
	public ResponseEntity<List<Customer>> getCustomersByCityLike(@PathVariable String city,
			@RequestParam(name = "page", required = false, defaultValue = "0") Integer page) {
		try {
			List<Customer> vCustomers = new ArrayList<Customer>();
			gICustomerRepository.findByCityLike(city, PageRequest.of(page, 2)).forEach(vCustomers::add);
			return new ResponseEntity<>(vCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/state/{state}")
	public ResponseEntity<List<Customer>> getCustomersByStateLike(@PathVariable String state,
			@RequestParam(name = "page", required = false, defaultValue = "0") Integer page) {
		try {
			List<Customer> vCustomers = new ArrayList<Customer>();
			gICustomerRepository.findByStateLike(state, PageRequest.of(page, 2)).forEach(vCustomers::add);
			return new ResponseEntity<>(vCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/country/{country}")
	public ResponseEntity<List<Customer>> getCustomersByCountryLike(@PathVariable String country,
			@RequestParam(name = "page", required = false, defaultValue = "0") Integer page) {
		try {
			List<Customer> vCustomers = new ArrayList<Customer>();
			gICustomerRepository.findByCountryLike(country, PageRequest.of(page, 6)).forEach(vCustomers::add);
			return new ResponseEntity<>(vCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/update/{country}")
	public ResponseEntity<Object> updateCountry(@PathVariable String country) {
		try {
			int vCustomer = gICustomerRepository.updateCountry(country);
			return new ResponseEntity<>(vCustomer, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/all")
	public ResponseEntity<List<Customer>> getAllCustomer() {
		try {
			return new ResponseEntity<>(gCustomerService.getAllCustomer(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/details/{id}")
	public ResponseEntity<Object> getCustomerById(@PathVariable Integer id) {
		Optional<Customer> vCustomerData = gICustomerRepository.findById(id);
		if (vCustomerData.isPresent()) {
			try {
				Customer vCustomer = vCustomerData.get();
				return new ResponseEntity<>(vCustomer, HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			Customer vCustomerNull = new Customer();
			return new ResponseEntity<>(vCustomerNull, HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/create")
	public ResponseEntity<Object> createCustomer(@Valid @RequestBody Customer paramCustomer) {
		try {
			return new ResponseEntity<>(gCustomerService.createCustomer(paramCustomer), HttpStatus.CREATED);
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Customer: " + e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateCustomere(@PathVariable Integer id,
			@Valid @RequestBody Customer paramCustomer) {
		Optional<Customer> vCustomerData = gICustomerRepository.findById(id);
		if (vCustomerData.isPresent()) {
			try {
				return new ResponseEntity<>(gCustomerService.updateCustomer(paramCustomer, vCustomerData),
						HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Customer: " + e.getCause().getCause().getMessage());
			}
		} else {
			Customer vCustomerNull = new Customer();
			return new ResponseEntity<>(vCustomerNull, HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/delete/{id}")
	private ResponseEntity<Object> deleteCustomerById(@PathVariable Integer id) {
		Optional<Customer> vCustomerData = gICustomerRepository.findById(id);
		if (vCustomerData.isPresent()) {
			try {
				gICustomerRepository.deleteById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} catch (Exception e) {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else {
			Customer vCustomerNull = new Customer();
			return new ResponseEntity<>(vCustomerNull, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/export/customers/excel")
	public void exportToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);

		List<Customer> customer = new ArrayList<Customer>();
		gICustomerRepository.findAll().forEach(customer::add);
		ExcelExporter excelExporter = new ExcelExporter(customer);
		excelExporter.export(response);
	}

}
