package com.devcamp.pizza365.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.repository.ICustomerRepository;
import com.devcamp.pizza365.repository.IPaymentRepository;
import com.devcamp.pizza365.service.PaymentService;

@RestController
@CrossOrigin
@RequestMapping("/payments")
public class PaymentController {
  @Autowired
  IPaymentRepository gIPaymentRepository;
  @Autowired
  ICustomerRepository gICustomerRepository;
  @Autowired
  PaymentService gPaymentService;

  @GetMapping("/all")
  public ResponseEntity<List<Payment>> getAllPayment() {
    try {
      return new ResponseEntity<>(gPaymentService.getAllPayment(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/create/{customerId}")
  public ResponseEntity<Object> createPayment(@PathVariable Integer customerId,
      @Valid @RequestBody Payment paramPayment) {
    Optional<Customer> vCustomerData = gICustomerRepository.findById(customerId);
    if (vCustomerData.isPresent()) {
      try {
        return new ResponseEntity<>(gPaymentService.createPayment(paramPayment, vCustomerData), HttpStatus.CREATED);
      } catch (Exception e) {
        return ResponseEntity.unprocessableEntity()
            .body("Failed to Create specified Payment: " + e.getCause().getCause().getMessage());
      }
    } else {
      Customer vCustomerNull = new Customer();
      return new ResponseEntity<>(vCustomerNull, HttpStatus.NOT_FOUND);
    }
  }

  @PutMapping("/update/{id}/{CustomerId}")
  public ResponseEntity<Object> updatePayment(@PathVariable Integer id, @PathVariable Integer CustomerId,
      @Valid @RequestBody Payment paramPayment) {
    Optional<Payment> vPaymentData = gIPaymentRepository.findById(id);
    if (vPaymentData.isPresent()) {
      Optional<Customer> vCustomerData = gICustomerRepository.findById(CustomerId);
      if (vCustomerData.isPresent()) {
        try {
          return new ResponseEntity<>(gPaymentService.updatePayment(paramPayment, vPaymentData, vCustomerData),
              HttpStatus.OK);
        } catch (Exception e) {
          return ResponseEntity.unprocessableEntity()
              .body("Failed to Update specified Payment: " + e.getCause().getCause().getMessage());
        }
      } else {
        Customer vCustomerNull = new Customer();
        return new ResponseEntity<>(vCustomerNull, HttpStatus.NOT_FOUND);
      }
    } else {
      Payment vPaymentNull = new Payment();
      return new ResponseEntity<>(vPaymentNull, HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/delete/{id}")
  private ResponseEntity<Object> deletePaymentById(@PathVariable Integer id) {
    Optional<Payment> vPaymentData = gIPaymentRepository.findById(id);
    if (vPaymentData.isPresent()) {
      try {
        gIPaymentRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      Payment vPaymentNull = new Payment();
      return new ResponseEntity<>(vPaymentNull, HttpStatus.NOT_FOUND);
    }
  }
}
