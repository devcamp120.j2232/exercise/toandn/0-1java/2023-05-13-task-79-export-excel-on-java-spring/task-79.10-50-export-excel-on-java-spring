package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.repository.IOrderRepository;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
  @Autowired
  IOrderRepository gIOrderRepository;

  public ArrayList<Order> getAllOrder() {
    ArrayList<Order> listOrder = new ArrayList<>();
    gIOrderRepository.findAll().forEach(listOrder::add);
    return listOrder;
  }

  public Order createOrder(Order pOrder,
      Optional<Customer> pCustomerData) {
    try {
      Order vOrder = new Order();
      vOrder.setOrderDate(pOrder.getOrderDate());
      vOrder.setRequiredDate(pOrder.getRequiredDate());
      vOrder.setShippedDate(pOrder.getShippedDate());
      vOrder.setStatus(pOrder.getStatus());
      vOrder.setComments(pOrder.getComments());
      vOrder.setCustomer(pCustomerData.get());
      Order vOrderSave = gIOrderRepository.save(vOrder);
      return vOrderSave;
    } catch (Exception e) {
      return null;
    }
  }

  public Order updateOrder(Order pOrder, Optional<Order> pOrderData,
      Optional<Customer> pCustomerData) {
    try {
      Order vOrder = pOrderData.get();
      vOrder.setOrderDate(pOrder.getOrderDate());
      vOrder.setRequiredDate(pOrder.getRequiredDate());
      vOrder.setShippedDate(pOrder.getShippedDate());
      vOrder.setStatus(pOrder.getStatus());
      vOrder.setComments(pOrder.getComments());
      vOrder.setCustomer(pCustomerData.get());
      Order vOrderSave = gIOrderRepository.save(vOrder);
      return vOrderSave;
    } catch (Exception e) {
      return null;
    }
  }
}
