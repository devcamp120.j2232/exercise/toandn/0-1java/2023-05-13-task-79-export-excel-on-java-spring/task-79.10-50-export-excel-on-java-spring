package com.devcamp.pizza365.service;

import java.util.ArrayList;
import java.util.Optional;

import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.IProductLineRepository;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class ProductLineService {
  @Autowired
  IProductLineRepository gIProductLineRepository;

  public ArrayList<ProductLine> getAllProductLine() {
    ArrayList<ProductLine> listProductLine = new ArrayList<>();
    gIProductLineRepository.findAll().forEach(listProductLine::add);
    return listProductLine;
  }

  public ProductLine createProductLine(ProductLine pProductLine) {
    try {
      ProductLine vProductLineSave = gIProductLineRepository.save(pProductLine);
      return vProductLineSave;
    } catch (Exception e) {
      return null;
    }
  }

  public ProductLine updateProductLine(ProductLine pProductLine, Optional<ProductLine> pProductLineData) {
    try {
      ProductLine vProductLine = pProductLineData.get();
      vProductLine.setProductLine(pProductLine.getProductLine());
      vProductLine.setDescription(pProductLine.getDescription());
      ProductLine vProductLineSave = gIProductLineRepository.save(vProductLine);
      return vProductLineSave;
    } catch (Exception e) {
      return null;
    }
  }

}
